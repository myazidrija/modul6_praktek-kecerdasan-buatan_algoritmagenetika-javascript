var cities = [];
var totalCities = 12;

var popSize = 500;
var population = [];
var fitness = [];

var recordDistance = Infinity;
var bestEver;
var currentBest;

var statusP;

function setup()
{
	createCanvas(800, 800);
	var order = [];
	for(var i=0; i < totalCities; i++)
	{
		var v = createVector(random(width), random(height/2));
		cities[i] = v;
		order[i] = i;
	}
	
	for(var i=0; i < popSize; i++)
	{
		population[i] = shuffle(order);
	}
	statusP = createP('').style('fontSize', '32pt');
}